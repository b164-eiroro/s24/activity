
// step 2
db.users.find({ 

	$or: [{
		firstName: {$regex: "s", $options: '$i'
		}
	},
		{lastName:{$regex: "d", $options: '$i'}
		}
	]
	},
	{_id:0,
		"firstName":1,
			"lastName":1,
	}
	)

// step 3
db.users.find({
	$and: [{
		department: "HR"
	},
	{ age: { $gt : 70 } }
	]
}
)
// step 4
db.users.find({
	$and: 
	[{
		firstName: {
			$regex: "e", $options: '$i'
		}
	},
	{age: 
		{$lte: 30}
	}
	]
}
)